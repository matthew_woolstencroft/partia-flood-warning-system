# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

def sorted_by_key(x, i, reverse=False):
    """For a list of lists/tuples, return list sorted by the ith
    component of the list/tuple, E.g.

    Sort on first entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 0)
      >>> [(1, 2), (5, 1)]

    Sort on second entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 1)
      >>> [(5, 1), (1, 2)]

    """

    # Sort by distance
    def key(element):
        return element[i]

    return sorted(x, key=key, reverse=reverse)
#importing this function from utils kept causing errors, so I put it here for now

from haversine import haversine

def stations_by_distance(stations, p): #Task 1B
    '''this function takes a list of stations as input, and returns a sorted
    list of tuples in the format (station, distance to station)'''
    distances = []
    for station in stations:
        distances.append((station.name,haversine(station.coord,p),station.town))
    return sorted_by_key(distances, 1)

def stations_within_radius(stations, centre, r): #Task 1C
    '''this function takes a list of stations as input, and returns a 
    list of all stations within a radius r of a geographic coordinate x''' 
    within_radius = []
    for station in stations:
        if haversine(station.coord,centre) < r:
           within_radius.append (station.name)
    print(within_radius)
    return within_radius

def rivers_with_station(stations): #Task 1D
    '''this function takes a list of stations as input, and returns a set
    of the rivers which have stations'''
    rivers = set()
    for station in stations:
        rivers.add(station.river)
    return sorted_by_key(list(rivers),0)

def stations_by_river(stations):
    station_dictionary = {}
    for station in stations:
        if station.river in station_dictionary:
            #river is already in dictionary
            station_dictionary[station.river].append(station.name)
        else:
            #river not already in dictionary, so add it
            station_dictionary[station.river] = [station.name]
    return station_dictionary

def rivers_by_station_number(stations, N): #Task 1E
    ''' this function takes a list of stations as input, and returns a
    sorted list of tuples in the format (river name, number of stations)'''
    river_all = {} 
    for station in stations:
        if station.river in river_all:
            river_all[station.river] += 1
        else:
            river_all[station.river] = 1
    river_all = sorted_by_key(river_all.items(),1, True) 
    # river_all returns a complete list of sorted tuples in the format (river name,number of stations) 
    
    ties = 0 # number of rivers with the same number of stations with the Nth entry 
    if N-1 < len(river_all): # in case if the N is very large 
        count = river_all[N-1][1] 
        while N < len(river_all) and count == river_all[N][1]:
            N += 1
            ties += 1
    print ('found', ties, 'ties') 
    return river_all[:N]

        
