# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        d += '\n {}'.format(self.latest_level) # remove later
        return d
    
    def typical_range_consistent(self): #Task 1F
        """Inputs a MonitoringStation and returns true (i.e. consistent) if typical range
        tuple is not empty AND lower level is below higher level"""
        return self.typical_range != None and self.typical_range[1] > self.typical_range[0]
    
    def relative_water_level(self):
        """Inputs a MonitoringStation and returns the fraction above the typical low of the current water level,
        returns None for inconsistent data"""
        if self.typical_range_consistent() and self.latest_level != None:
            return (self.latest_level - self.typical_range[0])/(self.typical_range[1] - self.typical_range[0])
        else:
            return None



def inconsistent_typical_range_stations(stations):
    """Inputs an array of [MonitoringStation] and returns sorted array of station 
    names with inconsistent typical ranges"""
    stations_list = []
    for station in stations:
        if MonitoringStation.typical_range_consistent(station) == False:
            stations_list.append(station.name)
    stations_list.sort()
    return stations_list
