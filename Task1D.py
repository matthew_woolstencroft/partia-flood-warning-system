from floodsystem.geo import rivers_with_station
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_river


stations = build_station_list()
print(rivers_with_station(stations)[:10])
print(len(rivers_with_station(stations)))
stations_dictionary = stations_by_river(stations)
print(stations_dictionary["River Aire"])
print(stations_dictionary["River Cam"])
print(stations_dictionary["River Thames"])