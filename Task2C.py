from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from flood import staions_highest_rel_level

def run():
    """Requirements for Task 2C"""

    # Build list of stations
    stations = build_station_list()
    N = 10
    
    staions_highest_rel_level(stations, N)


if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()

