import datetime

from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from analysis import polyfit
from plot import plot_water_level_with_fit
from flood import staions_highest_rel_level

stations = build_station_list()
update_water_levels(stations)
N = 5
stations_to_plot = staions_highest_rel_level(stations,N)
dt= 2
p = 4

#station_name = []
#for station in stations:
    #station_name.append(station.name)
#compare = [(name,selected_station)for name in station_name for selected_station in stations_to_plot]
#for elements in compare:
    #if elements[0]==elements[1][0]:
        #print(elements)
    
#compare_name=[]
#for stations1 in compare:
    #compare_name.append(stations1[1][0])
#print (compare_name)

for station in stations:
    if station.name == stations_to_plot[0][0] or station.name == stations_to_plot[1][0] or station.name == stations_to_plot[2][0] or station.name == stations_to_plot[3][0] or station.name == stations_to_plot[4][0]:
        dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
        plot_water_level_with_fit(station, dates, levels, p)
        polyfit(dates,levels,p) 
    else:
        pass

