from floodsystem.stationdata import build_station_list, update_water_levels
from flood import stations_level_over_threshold
stations = build_station_list()
update_water_levels(stations)
from plot import plot_station

stations_to_plot = stations_level_over_threshold(stations, 0.0)[:5]
for station in stations_to_plot:
    plot_station(station[0])
    