from floodsystem.station import MonitoringStation
from flood import stations_level_over_threshold
def test_relative_water_level():
    stations = [
        MonitoringStation("s", "m", "stat", (0,0), (0,2), "river1", "town1"),
        MonitoringStation("s", "m", "stat", (0,0), (0,2), "river2", "town2"),
        MonitoringStation("s", "m", "stat", (0,0), (0,2), "river3", "town3")
    ]
    stations[0].latest_level = 0.5
    stations[1].latest_level = 2.0
    stations[2].latest_level = 4.0
    
    stations_over_threshold_list = stations_level_over_threshold(stations, 0.5)
    assert stations_over_threshold_list[0][0].river == "river3"
    assert stations_over_threshold_list[1][0].river == "river2"