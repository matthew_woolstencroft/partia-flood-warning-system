# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_typical_range_consistent():
    #create stations
    stations = [
        MonitoringStation("s", "m", "stat", (0,0), (-2,2), "river", "town"),
        MonitoringStation("s", "m", "stat", (0,0), (2,-2), "river", "town"),
        MonitoringStation("s", "m", "stat", (0,0), None, "river", "town")
    ]
    assert MonitoringStation.typical_range_consistent(stations[0]) == True
    assert MonitoringStation.typical_range_consistent(stations[1]) == False
    assert MonitoringStation.typical_range_consistent(stations[2]) == False

def test_relative_water_level():
    stations = [
        MonitoringStation("s", "m", "stat", (0,0), (0,2), "river", "town"),
        MonitoringStation("s", "m", "stat", (0,0), (0,2), "river", "town"),
        MonitoringStation("s", "m", "stat", (0,0), (0,2), "river", "town")
    ]
    stations[0].latest_level = 0.5
    stations[1].latest_level = 2.0
    stations[2].latest_level = 4.0
    
    assert MonitoringStation.relative_water_level(stations[0]) == 0.25
    assert MonitoringStation.relative_water_level(stations[1]) == 1.0
    assert MonitoringStation.relative_water_level(stations[2]) == 2.0