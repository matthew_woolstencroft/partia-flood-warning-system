import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list,update_water_levels
from floodsystem.station import MonitoringStation
import numpy as np
import matplotlib
import matplotlib.dates

stations = build_station_list()
update_water_levels(stations)
dt=2

for station in stations:
    
    dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
    if dates == []:
        continue
    float_dates = matplotlib.dates.date2num(dates)
    p_coeff = np.polyfit(float_dates-float_dates[0], levels, 1)
    #print(p_coeff[1])
       
    if p_coeff[0] == None:
        print('Data not found')
    elif p_coeff[0] >= 0.009: #or station.name in second_measure[:10]:
        print(station.town + " is at severe risk of flooding")
    elif p_coeff[0] >= 0.006: #or station.name in second_measure[:20]:
        pass#print(station.town + "is at high risk of flooding")
    elif p_coeff[0] >= 0.004: #or station.name in second_measure[:30]:
        pass#print(station.town + "is at moderate risk of flooding")
    else:
        pass#print(station.town + "is at low risk of flooding")

