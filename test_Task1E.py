from floodsystem.stationdata import build_station_list
from floodsystem.utils import sorted_by_key
from floodsystem.geo import rivers_by_station_number

def test_river_by_station_number():
    assert rivers_by_station_number(build_station_list(),1)[0][0] == 'River Thames'