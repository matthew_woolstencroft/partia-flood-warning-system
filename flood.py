"""This module contains a selection of functions which assess flood risks"""
from floodsystem.station import MonitoringStation
from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import update_water_levels

def stations_level_over_threshold(stations, tol):
    station_levels = []
    for station in stations:
        if MonitoringStation.relative_water_level(station) != None:
                if MonitoringStation.relative_water_level(station) > tol:
                        station_levels.append((station, MonitoringStation.relative_water_level(station)))
    return sorted_by_key(station_levels,1,True)

def staions_highest_rel_level(stations, N): #Task 2C 
    ''' This function returns a list of N stations, at which the water 
    level is highest relative to the typical range'''
    
    update_water_levels(stations)
    relative_high_level = {}
    
    for station in stations:
        if station.latest_level != None and station.typical_range != None:
                water_level = station.latest_level - station.typical_range[1]  
                relative_high_level[station.name]= water_level
        else: 
                pass
    relative_high_level = sorted_by_key(relative_high_level.items(),1,True)
    
    for i in relative_high_level[:N]:
            print(i[0],i[1])
    return relative_high_level[:N]