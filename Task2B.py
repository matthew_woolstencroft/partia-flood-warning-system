from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels
from flood import stations_level_over_threshold
stations = build_station_list()
update_water_levels(stations)

stations_over_threshold_list = stations_level_over_threshold(stations, 0.8)
for station in stations_over_threshold_list:
    print(station[0].name,station[1])